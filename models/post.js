const mongoose = require( 'mongoose');
const { stringify } = require('querystring');

let Schema = mongoose.Schema;

const postSchema = Schema({
    title: String,
    content: String,
    user: String,
    created: { type: Date, default: Date.now },
});

const Post = mongoose.model('post', postSchema);

module.exports = Post;